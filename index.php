<?php

require_once('data.php');

session_start();

$resultat = isset($_SESSION['resultat']) ? $_SESSION['resultat'] : null;
$erreur = isset($_SESSION['erreur']) ? $_SESSION['erreur'] : null;
$depart = isset($_SESSION['depart']) ? $_SESSION['depart'] : null;
$arrivee = isset($_SESSION['arrivee']) ? $_SESSION['arrivee'] : null;

$itineraire = $resultat['itineraire'];
$distance = $resultat['distance'];

foreach($_SESSION as $k => $_) {
    unset($_SESSION[$k]);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Calcul d'itinéraires</title>
    <style>
        .error { color: red; }
    </style>
</head>
<body>
    <form method="POST" action="itineraire.php">
        <select name="depart">
            <option value="">-- Ville de départ --</option>
            <?php foreach($data['nodes'] as $ville): ?>
                <option value="<?= $ville ?>" <?= ($ville == $depart) ? 'selected="selected"' : '' ?>>
                    <?= $ville ?>
                </option>
            <?php endforeach; ?>
        </select>
        <select name="arrivee" placeholder="-- Ville d'arrivée --">
            <option value="">-- Ville d'arrivée --</option>
            <?php foreach($data['nodes'] as $ville): ?>
                <option value="<?= $ville ?>" <?= ($ville == $arrivee) ? 'selected="selected"' : '' ?>>
                    <?= $ville ?>
                </option>
            <?php endforeach; ?>
        </select>
        <button type="submit">Go !</button>
    </form>
    <?php
        $itineraire = $resultat['itineraire'];
        $distance = $resultat['distance'];
    ?>
    <?php if(!empty($erreur)): ?>
        <p class="error"><?= $erreur ?></p>
    <?php endif; ?>
    <?php if(!empty($itineraire)): ?>
        <h2>Résultat du calcul</h2>
        <p>Votre itineraire (le plus court) depuis <?= $depart ?> vers <?= $arrivee ?>
            fait <?= $distance ?> km de long</p>
        <p>Détail de l'itinéraire :</p>
        <ol>
            <?php foreach($itineraire as $ville): ?>
                <li><?= $ville ?></li>
            <?php endforeach; ?>
        </ol>
    <?php endif; ?>
</body>
