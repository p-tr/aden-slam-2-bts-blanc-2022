<?php
require_once('data.php');

$erreur = null;

$depart = $_POST['depart'] ? $_POST['depart'] : '';
$arrivee = $_POST['arrivee'] ? $_POST['arrivee'] : '';

$resultat = [
    'itineraire' => [],
    'distance' => 0,
];

$links = $data['links'];

session_start();

$_SESSION['depart'] = $depart;
$_SESSION['arrivee'] = $arrivee;

function walk(array $path) : array {
    global $links;
    $len = count($path);
    $pos = $path[$len - 1];
    $res = [];

    foreach(array_keys($links[$pos]) as $next) {
        if(!in_array($next, $path)) {
            $res[] = array_merge($path, [ $next ]);
        }
    }

    foreach($res as $p) {
        $r = walk($p);
        $res = array_merge($res, $r);
    }

    return $res;
}

function distance(array $path) : int {
    global $links;
    $distance = 0;
    for($i = 0; $i < (count($path) - 1); $i++) {
        $from = $path[$i];
        $to = $path[$i + 1];
        $distance += $links[$from][$to];
    }
    return $distance;
}

function search(array $res, string $to) : array {
    $results = [];
    foreach($res as $path) {
        $len = count($path);
        $index = implode(':', $path);
        if($path[$len - 1] == $to) {
            $results[$index] = distance($path);
        }
    }
    return $results;
}

if(empty($depart) || empty($arrivee)) {
    $erreur = "Vous devez choisir une ville de départ et une ville d'arrivée";
} else {
    $waypoints = walk([ $depart ]);
    $results = search($waypoints, $arrivee);

    $itineraire = [];
    $distance = 0;

    foreach($results as $p => $km) {
        if($distance == 0) {
            $distance = $km;
            $itineraire = explode(':', $p);
        } else {
            if($km < $distance) {
                $distance = $km;
                $itineraire = explode(':', $p);
            }
        }
    }

    $resultat['itineraire'] = $itineraire;
    $resultat['distance'] = $distance;

    if(!$distance) {
        $erreur = "Attention ! Vous avez choisi un itinéraire nul ou impossible !";
    }
}

$_SESSION['resultat'] = $resultat;
$_SESSION['erreur'] = $erreur;

header("Location: index.php");
